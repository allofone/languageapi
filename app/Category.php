<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public $incrementing = false;
    protected $casts = [
        'id' => 'string'
    ];
    
    public function questions()
    {
        return $this->hasMany('App\Question', 'idcategory');
    }
}
