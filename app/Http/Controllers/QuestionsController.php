<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Question;
use App\Answer;
use Faker\Provider\Uuid;
use Illuminate\Support\Facades\DB;

class QuestionsController extends Controller
{
    public function __construct() {
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $question = Question::with('category')->get();
        return response()->json($question, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        DB::beginTransaction();
        try{
            $quest = new Question;
            $quest->id = Uuid::uuid();
            $quest->idcategory = $request->input('idcategory');
            $quest->description = $request->input('description');
            $success = $quest->save();

            if (!is_null($request->input('answers'))) {
                foreach($request->input('answers') as $ans){
                    $answer = new Answer;            
                    $answer->id = Uuid::uuid();
                    $answer->description = $ans['description'];
                    $answer->istrue = $ans['istrue'];
                    $quest->answers()->save($answer);
                }
            }

            if (!$success){
                return response()->json('error saving', 500);
            }
            DB::commit();
            return response()->json('success');
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json('error', 500);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $quest = Question::with(array('answers','category'))->find($id);
        if (is_null($quest)){
            return response()->json("Not Found", 404);
        }
        return response()->json($quest, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        DB::beginTransaction();
        try{
            $quest   = Question::find($id);
            if (!is_null($request->input('idcategory'))){
                $quest->idcategory = $request->input('idcategory');
            }
            if (!is_null($request->input('description'))){
                $quest->description = $request->input('description');
            }
            $success = $quest->save();
            $quest->answers()->delete();

            if (!is_null($request->input('answers'))) {
                foreach($request->input('answers') as $ans){
                    $answer = new Answer;            
                    $answer->id = Uuid::uuid();
                    $answer->description = $ans['description'];
                    $answer->istrue = $ans['istrue'];
                    $quest->answers()->save($answer);
                }
            }
            
            if (!$success){
                return response()->json("Error Updating", 500);
            }
            DB::commit();
            return response()->json("success", 201);
        }catch (\Exception $e) {
            DB::rollback();
            return response()->json('error', 500);
        }   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
