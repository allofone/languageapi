<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    public $incrementing = false;
    protected $casts = [
        'id' => 'string',
        'idcategory' => 'string'
    ];
    
    public function answers()
    {
        return $this->hasMany('App\Answer', 'idquestion');
    }
    
    public function category()
    {
        return $this->belongsTo('App\Category', 'idcategory');
    }
}
