<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    public $incrementing = false;
    protected $casts = [
        'id' => 'string',
        'idquestion' => 'string'
    ];
    
    public function question()
    {
        return $this->belongsTo('App\Question', 'idquestion');
    }
}
