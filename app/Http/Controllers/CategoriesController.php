<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Category;
use Faker\Provider\Uuid;

class CategoriesController extends Controller
{
    public function __construct() {
        $this->middleware('jwt.auth', ['except' => ['authenticate']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $category = Category::all();
        return response()->json($category, 200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $category = new Category;
        $category->id = Uuid::uuid();
        $category->name = $request->input('name');
        $category->description = $request->input('description');
        $success = $category->save();
        if(!$success){
            return response()->json('error saving', 500);
        }
        return response()->json('success');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $category = Category::find($id);
        if (is_null($category)){
            return response()->json("Not Found", 404);
        }
        return response()->json($category, 200);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $category   = Category::find($id);
        if (!is_null($request->input('name'))){
            $category->name = $request->input('name');
        }
        if (!is_null($request->input('description'))){
            $category->description = $request->input('description');
        }
        $success = $category->save();

        if (!$success){
            return response()->json("Error Updating", 500);
        }
        return response()->json("success", 201);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $category   = Category::find($id);
        $category->delete();
    }
}
